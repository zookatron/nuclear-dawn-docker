FROM debian:11.6

# Install dependencies
RUN dpkg --add-architecture i386 && \
  apt update && \
  apt install -yq curl libcurl4:i386 wget file tar bzip2 gzip unzip bsdmainutils python3 util-linux ca-certificates \
  binutils bc jq tmux netcat lib32gcc-s1 lib32stdc++6 cpio distro-info xz-utils iproute2 procps sudo git dos2unix && \
  useradd -m nucleardawn
USER nucleardawn
WORKDIR /home/nucleardawn

# Install LinuxGSM
RUN wget -O linuxgsm.sh https://linuxgsm.sh && \
  chmod +x linuxgsm.sh && \
  bash linuxgsm.sh ndserver && \
  ./ndserver

# Download Nuclear Dawn
COPY ndserver.cfg /home/nucleardawn/ndserver.cfg
COPY check_deps.sh /home/nucleardawn/check_deps.sh
RUN --mount=type=secret,id=STEAM_USERNAME,mode=0777 --mount=type=secret,id=STEAM_PASSWORD,mode=0777 \
  cp ~/check_deps.sh ~/lgsm/functions/check_deps.sh && \
  rm ~/check_deps.sh && \
  cp ~/ndserver.cfg ~/lgsm/config-lgsm/ndserver/ndserver.cfg && \
  sed -i "s/STEAM_USERNAME/$(cat /run/secrets/STEAM_USERNAME)/g" ~/lgsm/config-lgsm/ndserver/ndserver.cfg && \
  sed -i "s/STEAM_PASSWORD/$(cat /run/secrets/STEAM_PASSWORD)/g" ~/lgsm/config-lgsm/ndserver/ndserver.cfg && \
  ./ndserver auto-install && \
  cp ~/ndserver.cfg ~/lgsm/config-lgsm/ndserver/ndserver.cfg && \
  rm ~/ndserver.cfg

# Configure Nuclear Dawn
COPY autoexec.cfg /home/nucleardawn/autoexec.cfg
COPY server.cfg /home/nucleardawn/server.cfg
COPY workshop.vdf /home/nucleardawn/workshop.vdf
RUN --mount=type=secret,id=GAME_SERVER_LOGIN_TOKEN,mode=0777 --mount=type=secret,id=SERVER_PASSWORD,mode=0777 --mount=type=secret,id=REMOTE_CONSOLE_PASSWORD,mode=0777 \
  cp ~/autoexec.cfg ~/serverfiles/nucleardawn/cfg/autoexec.cfg && \
  sed -i "s/GAME_SERVER_LOGIN_TOKEN/$(cat /run/secrets/GAME_SERVER_LOGIN_TOKEN)/g" ~/serverfiles/nucleardawn/cfg/autoexec.cfg && \
  sed -i "s/SERVER_PASSWORD/$(cat /run/secrets/SERVER_PASSWORD)/g" ~/serverfiles/nucleardawn/cfg/autoexec.cfg && \
  sed -i "s/REMOTE_CONSOLE_PASSWORD/$(cat /run/secrets/REMOTE_CONSOLE_PASSWORD)/g" ~/serverfiles/nucleardawn/cfg/autoexec.cfg && \
  rm ~/autoexec.cfg && \
  cp ~/server.cfg ~/serverfiles/nucleardawn/cfg/server.cfg && \
  rm ~/server.cfg && \
  cp ~/workshop.vdf ~/serverfiles/nucleardawn/workshop.vdf && \
  rm ~/workshop.vdf

# Install Metamod and SourceMod
COPY databases.cfg /home/nucleardawn/databases.cfg
RUN wget -O metamod.tar.gz https://mms.alliedmods.net/mmsdrop/1.11/mmsource-1.11.0-git1148-linux.tar.gz && \
  tar -xf metamod.tar.gz -C ~/serverfiles/nucleardawn/ && \
  rm metamod.tar.gz && \
  wget -O sourcemod.tar.gz https://sm.alliedmods.net/smdrop/1.11/sourcemod-1.11.0-git6927-linux.tar.gz && \
  tar -xf sourcemod.tar.gz -C ~/serverfiles/nucleardawn/ && \
  rm sourcemod.tar.gz && \
  wget -O steamworks.tar.gz https://github.com/KyleSanderson/SteamWorks/releases/download/1.2.3c/package-lin.tgz && \
  mkdir steamworks && \
  tar -xf steamworks.tar.gz -C steamworks && \
  cp -rf steamworks/package/addons ~/serverfiles/nucleardawn/ && \
  rm -r steamworks steamworks.tar.gz && \
  cp ~/databases.cfg ~/serverfiles/nucleardawn/addons/sourcemod/configs/databases.cfg && \
  rm ~/databases.cfg

# Install Redstone Plugins
RUN git clone https://github.com/stickz/Redstone.git && \
  cd Redstone && \
  chmod +x ./.github/scripts/build.sh && \
  ./.github/scripts/build.sh --sourcemod=1.11.0-6927 --out=build && \
  cp -rf build/updater/*/translations ~/serverfiles/nucleardawn/addons/sourcemod/ && \
  cp -rf build/updater/*/plugins ~/serverfiles/nucleardawn/addons/sourcemod/ && \
  cp -rf addons ~/serverfiles/nucleardawn/

# Configure Docker Image
COPY entrypoint /entrypoint
ENTRYPOINT /entrypoint
ARG REVISION
LABEL org.opencontainers.image.title="Nuclear Dawn Server Docker Container"
LABEL org.opencontainers.image.source="https://gitlab.com/zookatron/nuclear-dawn-docker"
LABEL org.opencontainers.image.revision=$REVISION
