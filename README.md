# Nuclear Dawn Server Docker Container

These are the source files for a Docker image that runs a functioning Nuclear Dawn server. It is not intended for use as a serious production quality server, it is mostly intended for documenting the general steps required to configure a Nuclear Dawn server for educational purposes.

## How to use:

1. You need to have a Steam account that has Steam Guard DISABLED and that owns [Nuclear Dawn](https://store.steampowered.com/app/17710/Nuclear_Dawn/).
2. You need to have a login token for a [Steam Game Server Account](https://steamcommunity.com/dev/managegameservers) with an App ID of 17710.
3. Open a terminal with the following environment variables set (using [direnv](https://direnv.net/) for this is recommended):
```
STEAM_USERNAME - Your steam account username
STEAM_PASSWORD - Your steam account password
GAME_SERVER_LOGIN_TOKEN - Your steam game server account login token
SERVER_PASSWORD - The player join password for the Nuclear Dawn server
REMOTE_CONSOLE_PASSWORD - The remote console password for the Nuclear Dawn server
CI_COMMIT_SHA - (optional) The version identifier for the image
```
4. Run `./run.sh` in your terminal to run the server in the foreground locally. You can use [Docker Compose](https://docs.docker.com/compose/) commands directly for more advanced use cases.
